# Lvm

 	

## Verificar si hay espacio disponible:

```bash
root@servidor:~# vgs
  VG     #PV #LV #SN Attr   VSize  VFree
  debian   1   6   0 wz--n- 79.76g 31.12g
```

Si hay espacio libre en VFree, se tienen que correr 2 comandos:

## Asignacion de espacio
```bash
    lvresize -L +XXG <path to fs device>
```

## Hace crecer el espacio online (usar segun file system)

```bash
    resi​ze2fs <path to fs device>​
    (para ext3, ext4)

    xfs_growfs -d <path to fs device>​
    (para xfs)
```

El primero asigna el espacio y el segundo hace crecer el espacio online.

Esto es para una maquina que ni siquiera tiene instalado LVM
```bash
    instalar lvm2
    presentar disco
    echo "- - -" > /sys/class/scsi_host/host0/scan
    echo "- - -" > /sys/class/scsi_host/host1/scan
    echo "- - -" > /sys/class/scsi_host/host2/scan
    pvcreate /dev/sd*  (extender vg -vgextend debian /dev/sdb-)
    vgcreate aliendata /dev/sd*
    lvcreate -L10G -naliendata-var aliendata
    mkfs.ext3 /dev/aliendata/aliendata-var
```
Esto para agrandar particiones en el virtualizador y luego las reescaneamos con lvm para q tome los nuevos tamaños
```bash
echo 1>/sys/class/block/sda/device/rescan
echo 1>/sys/class/block/sdb/device/rescan
```